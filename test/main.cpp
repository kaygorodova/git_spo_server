#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <vector>
#include <string>


#include <sys/wait.h>

using namespace std;
#define PORT 8080
#define MAX 6000

vector<vector <string> > pars_title(string title){
    vector<vector <string> > res;
    res.resize(2);
    int i = 0, j = 0;
    string s="";
    while(i<2){

        if(title[j] == '\r'){
            res[i].push_back(s);
            s="";
            j+=2;
            i++;
            continue;
        }

        if(title[j] != ' ')
            s+=title[j];
        else{
            res[i].push_back(s);
            s="";
        }
        j++;
    }
    
    return res;
}

int main()
{
    string dir = "";
    int socket_descriptor, ns, r; 
    struct sockaddr_in server_address, client_address;
    char buf[MAX];
    
    //Создание сокета
    socket_descriptor=socket(AF_INET,SOCK_STREAM,0);
    perror("soket");
    if (socket_descriptor<0)
        return 0;
         
    bzero(&server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(PORT);

    r=bind(socket_descriptor, (struct sockaddr *)&server_address, sizeof(server_address));
    perror("bind");
    if(r<0)
        return 0;

    r=listen(socket_descriptor, 10);
    perror("listen");
    if (r<0)
        return 0;
                                             
    while(1){

        bzero(&client_address, sizeof(client_address));
        int socket_descriptor_switching, pid;
        socklen_t address_len;
                                    
        socket_descriptor_switching=accept(socket_descriptor, (struct sockaddr *)&client_address, &address_len);
        perror("accept");

        if (socket_descriptor_switching>0){                                                                                 
            if (fork()==0){

                recv(socket_descriptor_switching, buf, sizeof(buf),0);
                perror("recv");
                
                vector<vector <string> > title;
                title=pars_title ((const char*) buf);

                int b, size, i=0;

                //FILE *in = fopen((dir+title[0][1]).c_str(),"rb");
                FILE *in =fopen("main.cpp", "rb");
                cout<<title[0][1]<<endl;
                while(!feof(in)) {
                b=fread(buf,1,sizeof(buf),in);
                size=ftell(in);
                printf("bytes read: %d, part:%d, pos: %ld \n",b,i,size);
                if(b!=0)
                    send(socket_descriptor_switching, buf, b,0);
                i++;
                }
                close(socket_descriptor_switching);
                cout<<"Hello"<<endl;
                
                return 0;
            }                
        }
        wait(NULL);    
        return 0;
    }
    return 0;
}
