#!/usr/bin/python2
# -*- coding:utf-8 -*-
import cgi
from datetime import datetime
import os
from os import environ

f = open("output.txt", 'w')
for i, j in environ.iteritems():
    f.write(i)
    f.write(' ')
    f.write(j)
    f.write('\n')

print "HTTP/1.1 200 OK\r\nServer: MyServer\r\n",
print "Content-Type: text/html; charset=utf-8\r\n\r\n"
print "<html><body>"

form = cgi.FieldStorage()
#print form
if 'newyear' in form:
    newyear = int(form['newyear'].value)
    interval = datetime(newyear, 1, 1) - datetime.now()
    print "<p>До нового %d года осталось %d дней</p>" % (newyear, interval.days)
else:
    print "<p>Текущее время: %s</p>" % datetime.now()

print "</body></html>"
