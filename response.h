#include <arpa/inet.h>
#include <iostream>
#include <sys/wait.h>
#include <map>
#include <string.h>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#define MAX 600000
#define SERVER "MyServer"

#ifndef RESPONSE_HPP
#define RESPONSE_HPP

using namespace std;

void sending_404(int socket);
string type_file(string name);
string create_answer(string name, string host);
void cgi_interface(string request, int socket, string type_request);
void sending_file(const char *name, string host, int socket, map <string, string> content_type);



#endif 
