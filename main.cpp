#include <sys/types.h>
#include "pars.h"
#include "response.h"

#define MAX 600000
#define SERVER "MyServer"

using namespace std;




/*struct conf_data{
    int port;
    map<string, string> content_type; 
    vector<string> static_files;
    vector<string> cgi_script;

};*/

//extern conf_data pars_conf();


bool type(string name, vector<string> path){

    int i, j;
    if(name=="/favicon.ico")
        return true;

    for(i=0; i<path.size(); i++){
        cout<<name<<endl<<path[i]<<endl<<endl;
        for(j=0; j<name.size(), j<path[i].size(); j++){
            if(name[j]!=path[i][j])
                break;
        }
        if(j==path[i].size() && (name.size()==j || name[j] == '/'))
            return true;
    }

    return false;

}

int main()
{
    cout<<SERVER<<endl;
    conf_data data;
    data = pars_conf(); 
    cout<<data.port<<endl;
    string dir = ".";
    int socket_descriptor, ns, r; 
    struct sockaddr_in server_address, client_address;
    char buf[MAX];
    
    //Создание сокета
    socket_descriptor=socket(AF_INET,SOCK_STREAM,0);
    perror("soket");
    if (socket_descriptor<0)
        return 0;
         
    bzero(&server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(data.port);

    r=bind(socket_descriptor, (struct sockaddr *)&server_address, sizeof(server_address));
    perror("bind");
    if(r<0)
        return 0;

    r=listen(socket_descriptor, 10);
    perror("listen");
    if (r<0)
        return 0;
                                             
    while(1){

        bzero(&client_address, sizeof(client_address));
        int socket_descriptor_switching, pid;
        socklen_t address_len;
                                    
        socket_descriptor_switching=accept(socket_descriptor, (struct sockaddr *)&client_address, &address_len);
        perror("accept");

        if (socket_descriptor_switching>0){                                                                                 
            if (fork()==0){

                close(socket_descriptor);

                recv(socket_descriptor_switching, buf, sizeof(buf),0);
                perror("recv");

                cout<<buf<<endl;

                //cout<<strlen(buf)<<endl;
                if(strlen(buf) == 0){
                    close(socket_descriptor_switching);
                    return 0;
                }
                
                vector<vector <string> > title;
                title=pars_title ((const char*) buf);

               
                if(type(title[0][1], data.static_files)){
		    cout<<"FILE"<<endl;
		    sending_file((dir+title[0][1]).c_str(), title[1][1], socket_descriptor_switching, data.content_type);
		    close(socket_descriptor_switching);                             
                    return 0;  
                }
                
                if(type(title[0][1], data.cgi_script)){
                     cout<<"CGI"<<endl;                
                     cgi_interface(dir+title[0][1], socket_descriptor_switching, title[0][0]);
                     close(socket_descriptor_switching);                             
                     return 0;                                                                  
                }

                sending_404(socket_descriptor_switching);

                close(socket_descriptor_switching);
                
                return 0;
            }
            close(socket_descriptor_switching);            
        }
           
    }
    close(socket_descriptor);
    return 0;
}
