#include "response.h"

void sending_404(int socket){
    
    int b;
    char buf[MAX];
    FILE *in;
    string title;
    title = "HTTP/1.1 404 Not Found\r\nServer: ";
    title += SERVER;
    title += "\r\nContent-Type: text/html\r\n\r\n";
    in = fopen("404.html", "rb");

    strcpy(buf, title.c_str());                                                 
    //cout<<buf<<endl;

    send(socket, buf, strlen(buf), 0);                                          
    while(!feof(in)){                                                          
        b=fread(buf,1,sizeof(buf),in);                                          
        if(b!=0)                                                                
            send(socket, buf, b, 0);                                            
    }                                                                           
                                                                                 
    fclose(in);  

    return;
}


string type_file(string name){
    string type;
    int i=name.size()-1;
    while(i>=0 && name[i] != '.' && name[i] != '/'){
        type = name[i] + type;
        i--;
    }
    //cout<<type<<endl;
    if(name == type)
        type = "";
    return type;
}
string create_answer(string name, string host){

    DIR *dir = opendir(name.c_str());                                           
    struct dirent *ent;
    string answer="";    
    
                                                   
    answer += "<html>\n  <body>\n";                                                  
    
    while((ent = readdir(dir)) != NULL){                                
        cout<<ent->d_name<<endl;

        answer +="      <a href = \"http://" + host + "/" + name;
        if(name[name.size()-1]!='/')
            answer += "/";
        answer += ent->d_name;
        answer += "\"> ";
        answer += ent->d_name;
        answer += " </a> <br/>\n";
    }                                                                   
    answer += "  </body>\n</html>\n";                                                   
    
    return answer;
}

void cgi_interface(string request, int socket, string type_request){
  
    string name;
    int i;
    for(i = 0; i<request.size(); i++){
      if(request[i] != '?')
	  name.push_back(request[i]);
      else
	break;
    }

    string query_string = "";
    for(i++; i<request.size(); i++){
      query_string+=request[i];
    }
  
    char** arg1;
    arg1 = new char*[2];
    arg1[0] = new char[50];
    strcpy(arg1[0], "cgi");
    arg1[1] = NULL;
    
    char** arg2;
    arg2 = new char*[4];
    for(i = 0; i < 3; i++)
        arg2[i] = new char [50];
    
    string buf="SERVER_SOFTWARE=";
    buf+=SERVER;
    strcpy(arg2[0], buf.c_str());
    buf = "REQUEST_METHOD=";
    buf += type_request;
    strcpy(arg2[1], buf.c_str());
    buf = "QUERY_STRING=";
    buf += query_string;
    strcpy(arg2[2], buf.c_str());
    arg2[3] = NULL;

    int new_socket = socket;
    
    if(fork() == 0){

        dup2(new_socket, 1);
        if(execve(name.c_str(), arg1, arg2) == -1){
            sending_404(new_socket);
        }
        return;
    }
    wait(NULL);

    return;
}

void sending_file(const char *name, string host, int socket, map <string, string> content_type){

    int b;
    char buf[MAX];
    FILE *in;
    string title;
    struct stat *options;
    stat(name, options);
    title = "HTTP/1.1 200 OK\r\nServer: ";
    title += SERVER;
    title += "\r\n";
    if(S_ISREG(options->st_mode)==true){
        map<string,string>::iterator i;                                        
        i = content_type.find(type_file((const char*)name));

        //cout<<type_file((const char*)name)<<endl;                             
        //title = "HTTP/1.1 200 OK\r\nServer: MyServer\r\n";                      
        
        if(i != content_type.end())                                             
            title += "Content-Type: " + (*i).second + "\r\n\r\n";              
        else                                                                    
            title += "\r\n";
        //cout<<title<<endl;  
        in = fopen(name, "rb");                                                                                                
    }
    else{
        if(S_ISDIR(options->st_mode)==true){
            string answer = create_answer(name, host);

            title = "HTTP/1.1 200 OK\r\nServer: ";
	    title += SERVER;
	    title += "\r\nContent-Type: text/html; charset=utf-8\r\n\r\n";
            title += answer;

            strcpy(buf, title.c_str());                                                
            cout<<buf<<endl;                                                            
            send(socket, buf, strlen(buf), 0);
            return; 

        }
        else{
            title = "HTTP/1.1 404 Not Found\r\nServer: ";
	    title += SERVER;
	    title =+ "\r\nContent-Type: text/html\r\n\r\n";
            in = fopen("404.html", "rb");
        }
    }    

    
    strcpy(buf, title.c_str());
    cout<<buf<<endl;
    send(socket, buf, strlen(buf), 0);
    while(!feof(in)){
        b=fread(buf,1,sizeof(buf),in);                                    
        if(b!=0)                                                          
            send(socket, buf, b, 0);                                        
    }

    fclose(in);    

    return;
}

