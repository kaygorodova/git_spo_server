#include "pars.h"


conf_data pars_conf(){
    conf_data data;
    fstream in;
    in.open("file.conf");
    string str;
    in>>str>>data.port;
    in>>str>>str;
  
    while(str != "cgi"){
	data.static_files.push_back(str);
	in>>str;
    }
    
    while(in>>str)
	data.cgi_script.push_back(str);
  
  return data;
}

map<string,string> known_content_type()
{
    fstream in;
    in.open("content_type.conf");

    map<string,string> content_type;
    string key, data;
    
    while(in>>key>>data)
        content_type[key] = data;

    in.close();
    /*map <string, string>::iterator cur;
    for (cur=content_type.begin(); cur!=content_type.end(); cur++)
    {
        cout<<(*cur).first<<": "<<(*cur).second<<endl;
    }*/
        
    return content_type;
}


conf_data conf(){                                                          
    
    conf_data data;                                                             
    data.port = 8080;                                                           
    data.content_type = known_content_type();  
    
    (data.static_files).resize(1);
    data.static_files[0] = "/test";
        
    (data.cgi_script).resize(1);
    data.cgi_script[0] = "/cgi";
                                                                                                       
    return data;                                                                
}


vector<vector <string> > pars_title(string title){
    vector<vector <string> > res;
    res.resize(2);
    int i = 0, j = 0;
    string s="";
    while(i<2){

        if(title[j] == '\r'){
            res[i].push_back(s);
            s="";
            j+=2;
            i++;
            continue;
        }

        if(title[j] != ' ')
            s+=title[j];
        else{
            res[i].push_back(s);
            s="";
        }
        j++;
    }
    
    return res;
}