#include <vector>
#include <map>
#include <fstream>
#ifndef PARS_HPP
#define PARS_HPP
using namespace std;

struct conf_data{
    int port;
    map<string, string> content_type; 
    vector<string> static_files;
    vector<string> cgi_script;

};

conf_data pars_conf();
map<string,string> known_content_type();
conf_data conf();
vector<vector <string> > pars_title(string title);

#endif
